﻿using TMPro;
using UnityEngine;

namespace Scriptes.Joueurs
{
    public class NumbersofPlayers : MonoBehaviour
    {
        public int players = 0;

        // public int nombredejoueurs;
        private void Update()
        {
            // Debug.Log(players);
        }

        public void Playerschoics()
        {
            // Recupere la valeur du text et parse player
            TextMeshProUGUI text = GetComponentInChildren<TextMeshProUGUI>();
            players = int.Parse(text.text);

            PlayerPrefs.SetInt("player", players);
        }
    }
}