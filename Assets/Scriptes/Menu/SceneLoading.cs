﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoading : MonoBehaviour
{
    public string sceneaLoad;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void LoadScene()
    {
        // charge une scene
        SceneManager.LoadScene(sceneaLoad);
    }

    public void Quitt()
    {
        // quitte le jeu
        Application.Quit();
    }
}