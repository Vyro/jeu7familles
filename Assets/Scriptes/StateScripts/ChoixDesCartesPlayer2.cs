﻿using System.Collections;
using System.Linq;
using Scriptes.inGame;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChoixDesCartesPlayer2 : State
{
    public ChoixDesCartesPlayer2(GameManager gameManager) : base(gameManager)
    {
    }
     public override IEnumerator ResetButtonValue()
    {// On reset la valeur des buttons
        _gameManager.buttonjoueurs.interactable = true;
        _gameManager.buttonjoueurs.image.color = Color.white;
        _gameManager.buttonfamille.interactable = false;
        _gameManager.buttonfamille.image.color = Color.white;
        _gameManager.buttonfamille.GetComponentInChildren<TextMeshProUGUI>().text = "Quelle Famille :";
        _gameManager.buttonmembre.interactable = false;
         _gameManager.buttonmembre.image.color = Color.white;
         _gameManager.buttonmembre.GetComponentInChildren<TextMeshProUGUI>().text = "Quel Membre :";
         _gameManager.buttondemander.interactable = false;
         _gameManager.buttonjoueurs.GetComponentInChildren<TextMeshProUGUI>().text = "Joueur 1";
         foreach (Button buttonmembre in _gameManager.buttonMembrenumber)
         {
             buttonmembre.interactable = true;
         }
        yield break;
    }

    public override IEnumerator Start()
    {// Active le panel pour le choix des cartes a demander
        _gameManager.panelchoixcartes.SetActive(true);
       _gameManager.panelChoixFamilles.SetActive(false);
       _gameManager.panelChoixMembres.SetActive(false);
      
       yield break;
    }

    public override IEnumerator ExitPannel()
    {//Quit le pannel et reviens sur le state du joueur
        _gameManager.panelchoixcartes.SetActive(false); 
        _gameManager.SetState(new Player2TurnState(_gameManager));
        yield break;
    }

    public override IEnumerator DemandezCartesJoueur()
    {// Choix du joueur a qui on demande une carte
        
        _gameManager.buttonfamille.interactable = true;
        _gameManager.buttonjoueurs.interactable = false;
        yield break;
    }

    public override IEnumerator ButtonChoixFamille()
    {
        _gameManager.panelChoixMembres.SetActive(false);
        foreach (Card cardmain in _gameManager.joueurs[1].GetComponent<Joueur>().carteMainJoueur)
        {// Check si on a la carte de la famille pour ensuite afficher le button correspondant
            switch (cardmain.color)
            {
                case Colors.Blue : 
                    _gameManager.buttonFamillesCouleurs[0].interactable = true;
                    break;
                case Colors.Gray :
                    _gameManager.buttonFamillesCouleurs[1].interactable = true;
                    break;
                case  Colors.Green :
                    _gameManager.buttonFamillesCouleurs[2].interactable = true;
                    break;
                case Colors.Magenta :
                    _gameManager.buttonFamillesCouleurs[3].interactable = true;
                    break;
                case  Colors.Red :
                    _gameManager.buttonFamillesCouleurs[4].interactable = true;
                    break;
                case Colors.White :
                        _gameManager.buttonFamillesCouleurs[5].interactable = true;
                        break;
                case Colors.Yellow :
                    _gameManager.buttonFamillesCouleurs[6].interactable = true;
                    break;
            }
        }
        _gameManager.panelChoixFamilles.SetActive(true);
        
        yield break;
    }
    
    public override IEnumerator ChoixFamillesCouleurs(int numberColor)
    { //Quand on appuie sur le button de la famille blue par exemple cela prend son text et change le text du buttonfamille pour dire que c'est la famille blue qui est sélectionner
        string buttontext = _gameManager.buttonFamillesCouleurs[numberColor].GetComponentInChildren<TextMeshProUGUI>().text;
        _gameManager.buttonfamille.GetComponentInChildren<TextMeshProUGUI>().text = buttontext;
        _gameManager.textButtonFamille = buttontext;
        _gameManager.panelChoixFamilles.SetActive(false);
        _gameManager.buttonfamille.image.color = Color.yellow;
        _gameManager.buttonmembre.interactable = true;
        yield break;
    }

    public override IEnumerator ButtonChoixMembre()
    {  
        _gameManager.panelChoixFamilles.SetActive(false);
        foreach (Button button in _gameManager.buttonMembrenumber)
        {
            
            foreach (Card cardmain in _gameManager.joueurs[1].GetComponent<Joueur>().carteMainJoueur)
            {// Pour chaque carte que le joueur possede on va checker si la carte a le nombre "1" par exemple et si elle de la famille "" donc si le joueur a une de ces cartes on désactive l'intéraction du bouton
                if (int.Parse(button.GetComponentInChildren<TextMeshProUGUI>().text) == cardmain.number &&  cardmain.color.ToString() == _gameManager.textButtonFamille )
                {
                    Debug.Log(cardmain.color.ToString());
                    Debug.Log(cardmain.number);
                    button.interactable = false;
                }
            }
        }
        _gameManager.panelChoixMembres.SetActive(true);
        yield break;
    }

    public override IEnumerator ChoixMembreNumber(int numberMembre)
    {
        string buttontext = _gameManager.buttonMembrenumber[numberMembre].GetComponentInChildren<TextMeshProUGUI>().text;
        _gameManager.buttonmembre.GetComponentInChildren<TextMeshProUGUI>().text = buttontext;
        _gameManager.textButtonMembre = buttontext;
        _gameManager.panelChoixMembres.SetActive(false);
        _gameManager.buttonmembre.image.color = Color.yellow;
        _gameManager.buttondemander.interactable = true;
        yield break;
    }


    public override IEnumerator Demander()
    {// Validation de la demande
        bool cartetrouvée = false;
        if (!cartetrouvée)
        {
            foreach (Card cardmainplayer2 in _gameManager.joueurs[0].GetComponent<Joueur>().carteMainJoueur.ToList())
            {// on check chaque carte qu'a le player 2 et si il y' a une carte qui correspond a notre demande on l'ajoute dans la list de cartes du player1
                if (int.Parse(_gameManager.textButtonMembre) == cardmainplayer2.number && cardmainplayer2.color.ToString() == _gameManager.textButtonFamille)
                {
                    _gameManager.joueurs[1].GetComponent<Joueur>().carteMainJoueur.Add(cardmainplayer2);
                    _gameManager.MajDisplayCards(cardmainplayer2, 0);
                    _gameManager.joueurs[0].GetComponent<Joueur>().carteMainJoueur.Remove(cardmainplayer2);
                    cartetrouvée = true;
                    _gameManager.textresultdemande.color = Color.green;
                    _gameManager.textresultdemande.text = "Bonne pioche ! Le joueur 2 rejoue";
                    yield return new WaitForSeconds(2f);
                    _gameManager.panelchoixcartes.SetActive(false);
                    _gameManager.buttonjoueurs.interactable = true;
                    _gameManager.textresultdemande.text = "";
                    _gameManager.SetState(new Player2TurnState(_gameManager));
                    yield break;
                }
            }
        }
        
        if (!cartetrouvée)
        {// si le player 2 n'a pas la carte que le player 1 demande alors le player 1 prendra une carte de la pioche
            Card random = _gameManager.deck[Random.Range(0, _gameManager.deck.Count)];
            _gameManager.joueurs[1].GetComponent<Joueur>().carteMainJoueur.Add(random);
            _gameManager.MajDisplayCards(random, 0);
            _gameManager.deck.Remove(random);
            _gameManager.nbcartepioche = _gameManager.deck.Count;
            _gameManager.pioche.GetComponentInChildren<TextMeshProUGUI>().text = _gameManager.nbcartepioche.ToString();
            _gameManager.textresultdemande.color = Color.red;
            _gameManager.textresultdemande.text = "Le joueur 2 pioche";
        }
         yield return new WaitForSeconds(2f);
        _gameManager.panelchoixcartes.SetActive(false);
        _gameManager.buttonjoueurs.interactable = true;
        _gameManager.textresultdemande.text = "";
        _gameManager.SetState(new PlayerTurnState(_gameManager));
        yield break;
    }
}