using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using Scriptes.inGame;
using Scriptes.Joueurs;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class DistributionState : State
{
    public DistributionState(GameManager gameManager) : base(gameManager)
    {
    }

    public override IEnumerator Start()
    {
        int nombresdejoueurs = PlayerPrefs.GetInt("player");
        GameObject cardCanvas;
        List<Canvas> canvasList = new List<Canvas>();

        for (int i = 0; i < nombresdejoueurs; i++)
        {
            GameObject gameObject = GameObject.Instantiate(_gameManager.players);
            _gameManager.joueurs.Add(gameObject);
            Canvas canvas = GameObject.Instantiate(_gameManager.canvasMainJoueur);
            canvasList.Add(canvas);
        }

        _gameManager.SetListCanvas(canvasList);

        // Permet de distribuer 7 cartes dans la liste carteMainjoueur et remove de la liste cartesDistribution
        int numJoueur = 0; // le nombre de joueurs va de 1 à 4
        foreach (GameObject joueur in _gameManager.joueurs)
        {
            for (int i = 0; i < _gameManager.mainjoueur; i++)
            {
                Card random = _gameManager.cartesDistribution[Random.Range(0, _gameManager.cartesDistribution.Count)];

                _gameManager.joueurs[numJoueur].GetComponent<Joueur>().carteMainJoueur.Add(random);
                _gameManager.cartesDistribution.Remove(random);
            }

            numJoueur++;
        }
        
        // Pour chaque joueur existant
        for (int i = 0; i < nombresdejoueurs; i++)
        {
            // Affiche les cartes de la main du joueur à l'écran
            foreach (Card cardMain in _gameManager.joueurs[i].GetComponent<Joueur>().carteMainJoueur)
            {
                _gameManager.MajDisplayCards(cardMain, i);
            }
        }
        

        foreach (Card carddeck in _gameManager.cartesDistribution)
        {
            // Ajoute les cartes restantes dans cartesDistribution a la list deck
            _gameManager.deck.Add(carddeck);
        }

        int nbcartes = int.Parse(_gameManager.pioche.GetComponentInChildren<TextMeshProUGUI>().text);
        nbcartes = _gameManager.deck.Count;
        _gameManager.nbcartepioche = nbcartes;
        _gameManager.pioche.GetComponentInChildren<TextMeshProUGUI>().text = _gameManager.nbcartepioche.ToString();
        // réinitialisation count
        _gameManager.SetState(new PlayerTurnState(_gameManager));
        yield break;
    }
}