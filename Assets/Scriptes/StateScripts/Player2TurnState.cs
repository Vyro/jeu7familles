﻿using System.Collections;
using Scriptes.inGame;
using TMPro;
using UnityEngine;

public class Player2TurnState : State
{
    public Player2TurnState(GameManager gameManager) : base(gameManager)
    {
        
    }

    public override IEnumerator Start()
    {// Permet de dire que c'est le joueur 2 qui joue
        _gameManager.paneltourDuJoueur.SetActive(true);
        TextMeshProUGUI text = _gameManager.paneltourDuJoueur.GetComponentInChildren<TextMeshProUGUI>();
        text.text  = "Tour du Joueur 2";
        _gameManager.playerturn.text = "Tour du Joueur 2";
        yield return new WaitForSeconds(2f);
        _gameManager.paneltourDuJoueur.SetActive(false);
        // Le canvas de la scène prend la valaur du canvas 2
        _gameManager.GetListCanvas()[1].gameObject.SetActive(true);
        _gameManager.GetListCanvas()[0].gameObject.SetActive(false);
        _gameManager.FamilyPointManage(1);
        _gameManager.ClearCanvas(_gameManager.GetListCanvas()[1].transform);
        // Met à jour l'affichage des cartes
        foreach (Card card in _gameManager.joueurs[1].GetComponent<Joueur>().carteMainJoueur)
        {
            _gameManager.MajDisplayCards(card, 1);
        }
        yield break;
    }

    public override IEnumerator Pause()
    {//Active le menu Pause
        Time.timeScale = 0f;
        _gameManager.pausePannel.SetActive(true);
        yield break;
    }

    public override IEnumerator Resume()
    {//Reprend le cour du jeu
        Time.timeScale = 1f;
        _gameManager.pausePannel.SetActive(false);
        yield break;
    }

    public override IEnumerator  PiocheDeCartes()
    {
        _gameManager.SetState(new ChoixDesCartesPlayer2(_gameManager));
        yield break;
    }
}