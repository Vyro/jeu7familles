﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Scriptes.inGame;
using TMPro;
using UnityEngine;

public class PlayerTurnState : State
{
    public PlayerTurnState(GameManager gameManager) : base(gameManager)
    {
    }


    public override IEnumerator Start()
    {
        // Permet de dire que c'est le joueur 1 qui joue
        _gameManager.paneltourDuJoueur.SetActive(true);
        TextMeshProUGUI text = _gameManager.paneltourDuJoueur.GetComponentInChildren<TextMeshProUGUI>();
        text.text = "Tour du Joueur 1";
        _gameManager.playerturn.text = "Tour du Joueur 1";
        yield return new WaitForSeconds(2f);
        _gameManager.paneltourDuJoueur.SetActive(false);
        // Le canvas de la scène prend la valaur du canvas 1
        _gameManager.GetListCanvas()[0].gameObject.SetActive(true);
        _gameManager.GetListCanvas()[1].gameObject.SetActive(false);
        _gameManager.FamilyPointManage(0);
        _gameManager.ClearCanvas(_gameManager.GetListCanvas()[0].transform);
        // Met à jour l'affichage des cartes
        foreach (Card card in _gameManager.joueurs[0].GetComponent<Joueur>().carteMainJoueur)
        {
            _gameManager.MajDisplayCards(card, 0);
        }
        yield break;
    }

    

    public override IEnumerator Pause()
    {
        //Active le menu Pause
        Time.timeScale = 0f;
        _gameManager.pausePannel.SetActive(true);
        yield break;
    }

    public override IEnumerator Resume()
    {
        //Reprend le cour du jeu
        Time.timeScale = 1f;
        _gameManager.pausePannel.SetActive(false);
        yield break;
    }

    public override IEnumerator PiocheDeCartes()
    {
        _gameManager.SetState(new ChoixFamillesCouleursEtMembres(_gameManager));
        yield break;
    }
}