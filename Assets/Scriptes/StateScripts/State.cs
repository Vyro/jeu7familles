﻿using System.Collections;
using System.Collections.Generic;
using Scriptes.inGame;
using UnityEngine;

public abstract class State
{
    protected  GameManager _gameManager;

    public State(GameManager gameManager)
    {
        _gameManager = gameManager;
    }

    public virtual IEnumerator Start()
    {
        yield break;
    }
    public virtual IEnumerator Pause()
    {
        yield break;
    }

    public virtual IEnumerator Resume()
    {
        yield break;
    }

    public virtual IEnumerator PiocheDeCartes()
    {
        yield break;
    }

    public virtual IEnumerator DemandezCartesJoueur()
    {
        yield break;
    }

    public virtual IEnumerator Demander()
    {
        yield break;
    }

    public virtual IEnumerator ButtonChoixFamille()
    {
        yield break;
    }

    public virtual IEnumerator ButtonChoixMembre()
    {
        yield break;
    }

    public virtual IEnumerator ExitPannel()
    {
        yield break;
    }

    public virtual IEnumerator ChoixFamillesCouleurs(int numberFamille)
    {
        yield break;
    }

    public virtual IEnumerator ChoixMembreNumber(int numberMembre)
    {
        yield break;
    }

    public virtual IEnumerator ResetButtonValue()
    {
        yield break;
    }
}