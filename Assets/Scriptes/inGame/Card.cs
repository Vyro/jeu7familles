﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scriptes.inGame
{
    [CreateAssetMenu(fileName = "New Card", menuName = "Card")]
    public class Card : ScriptableObject
    {
        // Numéro de la carte (1 à 6)
        public int number;
        // Couleur (Enum)
        public Colors color;
    }

    public enum Colors
    {
        Red,
        Green,
        Blue,
        Yellow,
        Magenta,
        White,
        Gray
    }
}