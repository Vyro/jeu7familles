using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Scriptes.inGame;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;


public class CardDisplay : MonoBehaviour
{
    public Card card;
    public Text numberText; 
    public string name;
    public GameObject cardTemplate;
    public Color colorCard; 

    void Start()
    {
        Setcolors();
        numberText.text = card.number.ToString();
        cardTemplate.GetComponent<Image>().color = colorCard;
    }

    void Setcolors()
    {
        if (card != null)
        {
            switch (card.color)
            {
                case Colors.Red:
                    colorCard = Color.red;
                    name = card.number + "Red";
                    break;

                case Colors.Green:
                    colorCard = Color.green;
                    name = card.number + "Green";
                    break;
                case Colors.Blue:
                    colorCard = Color.blue;
                    name = card.number + "Blue";
                    break;

                case Colors.Yellow:
                    colorCard = Color.yellow;
                    name = card.number + "Yellow";
                    break;
                case Colors.Magenta:
                    colorCard = Color.magenta;
                    name = card.number + "Pink";
                    break;
                case Colors.White:
                    colorCard = Color.white;
                    name = card.number + "White";
                    break;

                case Colors.Gray:
                    colorCard = Color.gray;
                    name = card.number + "Gray";
                    break;
            }
        }
        else
        {
           Debug.LogWarning("card = null");
        }
        
    }
}