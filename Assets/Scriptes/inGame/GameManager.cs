using System;
using System.Collections.Generic;
using System.Linq;
using Scriptes.Joueurs;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scriptes.inGame
{
    public class GameManager : StateMachine
    {
        //Concerne la pioche 
        public GameObject pioche;
        public int nbcartepioche;
        public int mainjoueur = 7;


        public List<Card> cartesDistribution;
        public List<Card> carteMainjoueur;
        public List<Card> deck;
        public GameObject cardPrefab;
        public Canvas canvasMainJoueur;
        public State currentstate;
        public TextMeshProUGUI textresultdemande;

        public GameObject pausePannel;
        public GameObject paneltourDuJoueur;
        public string menuload;
        public List<GameObject> joueurs;

        public GameObject players;
        public TextMeshProUGUI playerturn;
        // Concerne le panel choix des cartes avec ces buttons
        public GameObject panelchoixcartes;
        public Button buttonfamille;
        public Button buttonmembre;
        public Button buttondemander;

        public Button buttonjoueurs;

        // Concerne le panel choix des familles avec ces différents buttons
        public GameObject panelChoixFamilles;
        public List<Button> buttonFamillesCouleurs;

        public string textButtonFamille;
        [SerializeField]
        public List<GameObject> listImgFamily;
        // Concerne le panel choix des membres de la familles avec ces différents buttons
        public GameObject panelChoixMembres;
        public List<Button> buttonMembrenumber;
    
        public string textButtonMembre;

        // Listes des canvas pour afficher les cartes des joueurs
        private List<Canvas> canvasList;

        // Start is called before the first frame update
        public void SetState(State state)
        {
            currentstate = state;
            StartCoroutine(currentstate.Start());
        }

        public void Awake()
        {
            buttonjoueurs.interactable = true;
            buttonfamille.interactable = false;
            buttonmembre.interactable = false;
            buttondemander.interactable = false;
            Time.timeScale = 1f;
            Distribution();
        }

        public void Distribution()
        {
            SetState(new DistributionState(this));
        }

        public void PiocheDeCarte()
        {
            StartCoroutine(currentstate.PiocheDeCartes());
        }

        public void OnApplicationQuit()
        {
            Application.Quit();
        }

        public void RetourMenu()
        {
            SceneManager.LoadScene(menuload);
        }

        public void Resume()
        {
            StartCoroutine(currentstate.Resume());
        }

        public void Pause()
        {
            StartCoroutine(currentstate.Pause());
        }

        public void DemandezCartesJoueu1234()
        {
            StartCoroutine(currentstate.DemandezCartesJoueur());
        }

        public void Demander()
        {
            StartCoroutine(currentstate.Demander());
        }

        public void ButtonFamille()
        {
            StartCoroutine(currentstate.ButtonChoixFamille());
        }

        public void ButtonMembre()
        {
            StartCoroutine(currentstate.ButtonChoixMembre());
        }

        public void ExitPanelCartes()
        {
            StartCoroutine(currentstate.ExitPannel());
        }

        public void FamilleCouleursButtons(int numberColor)
        {
            StartCoroutine(currentstate.ChoixFamillesCouleurs(numberColor));
        }

        public void ChoixNumberMembre(int numberMembre)
        {
            StartCoroutine(currentstate.ChoixMembreNumber(numberMembre));
        }

        public void ResetDesButtons()
        {
            StartCoroutine(currentstate.ResetButtonValue());
        }

        void Start()
        {
            
        }

        /**
        * Permet de vérifier si la main du joueur possède une famille
        */
        public void FamilyPointManage(int numberJoueur)
        {
            var MainJoueurFiltre = joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.ToList().GroupBy(x => x.color)
                .Select(x => new
                {
                    Count = x.Count(),
                    Name = x.Key,
                    Number = x.First().number
                })
                .OrderByDescending(x => x.Count);
            
            // Pour chaque élément dans la main du joueur
            foreach (var x in MainJoueurFiltre)
            {
                Debug.Log("Count: " + x.Count + ", Family: " + x.Name);
                // Si l'on détecte 6 cartes de la même couleur
                if (x.Count == 6)
                {
                    Debug.Log("QUELQU'UN  A UNE FAMILLE !");
                    Debug.Log("NOM DE LA FAMILLE :" + x.Name);
                    // Selon la couleur
                    switch (x.Name)
                    {
                        case Colors.Blue:
                            //GameObject.FindGameObjectWithTag("Img_bleu").SetActive(true);
                            listImgFamily[0].SetActive(true);
                            foreach (Card card in joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.ToList())
                            {
                                if (card.color.Equals(Colors.Blue))
                                {
                                    joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.Remove(card);
                                }
                            }
                            break;
                        case Colors.Gray:
                            //GameObject.FindGameObjectWithTag("Img_gris").SetActive(true);
                            listImgFamily[4].SetActive(true);
                            GameObject.Find("Img_gris").SetActive(true);
                            foreach (Card card in joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.ToList())
                            {
                                if (card.color.Equals(Colors.Gray))
                                {
                                    joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.Remove(card);
                                }
                            }
                            break;
                        case Colors.Green:
                            //GameObject.FindGameObjectWithTag("Img_vert").SetActive(true);
                            listImgFamily[3].SetActive(true);
                            foreach (Card card in joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.ToList())
                            {
                                if (card.color.Equals(Colors.Green))
                                {
                                    joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.Remove(card);
                                }
                            }
                            break;
                        case Colors.Magenta:
                            // GameObject.FindGameObjectWithTag("Img_rose").SetActive(true);
                            listImgFamily[6].SetActive(true);
                            foreach (Card card in joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.ToList())
                            {
                                if (card.color.Equals(Colors.Magenta))
                                {
                                    joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.Remove(card);
                                }
                            }
                            break;
                        case Colors.Red:
                            //GameObject.FindGameObjectWithTag("Img_rouge").SetActive(true);
                            listImgFamily[1].SetActive(true);
                            foreach (Card card in joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.ToList())
                            {
                                if (card.color.Equals(Colors.Red))
                                {
                                    
                                    bool isRemoveSuccess = joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.Remove(card);
                                    Debug.Log("Suppression carte " + Colors.Red + " : " + isRemoveSuccess);
                                }
                            }
                            break;
                        case Colors.Yellow:
                            //GameObject.FindGameObjectWithTag("Img_jaune").SetActive(true);
                            listImgFamily[2].SetActive(true);
                            foreach (Card card in joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.ToList())
                            {
                                if (card.color.Equals(Colors.Yellow))
                                {
                                    joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.Remove(card);
                                }
                            }
                            break;
                        case Colors.White:
                            //GameObject.FindGameObjectWithTag("Img_blanc").SetActive(true);
                            listImgFamily[5].SetActive(true);
                            foreach (Card card in joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.ToList())
                            {
                                if (card.color.Equals(Colors.White))
                                {
                                    joueurs[numberJoueur].GetComponent<Joueur>().carteMainJoueur.Remove(card);
                                }
                            }
                            break;
                    }
                }
            }
        }

        public void ClearCanvas(Transform transform)
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }
        /**
         * Affiche les cartes du joueur 'i' à l'écran
         */
        public void MajDisplayCards(Card cardJoueur, int i)
        {
            // Récupérartion du préfab
            GameObject card = GameObject.Instantiate(cardPrefab);
            card.SetActive(true);
            // Le component 'CardDisplay' de card prend la valeur de cardMain
            card.GetComponent<CardDisplay>().card = cardJoueur;
            // On ajoute card au canvas afin de l'afficher à l'écran
            //card.transform.parent = GetListCanvas()[i].transform;
            card.transform.SetParent(GetListCanvas()[i].transform);
        }

        public List<Canvas> GetListCanvas()
        {
            return canvasList;
        }

        public void SetListCanvas(List<Canvas> _canvasList)
        {
            canvasList = _canvasList;
        }
    }
}